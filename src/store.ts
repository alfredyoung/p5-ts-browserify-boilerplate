import * as redux from 'redux';
import * as R from 'ramda';
import * as _ from 'lodash';

import { NUM_BODIES } from './constants';
import * as Cell from './cell';

interface IState {
    cells: Cell.ICell[];
}

const initialState: IState = {
    cells: R.map(
        Cell.random,
        R.range(0, NUM_BODIES),
    ),
};

const reducer = ({ cells }: IState, action) => {
    const filtered = R.filter((c: Cell.ICell) => c.health > 0, cells);
    const withChildren = R.reduce(
        (acc: Cell.ICell[], c: Cell.ICell) =>
            filtered.length > 3
                && filtered.length < 50
                ? _.random(0, 100) < 50
                    ? [...acc, Cell.of(c.body.pos[0] + _.random(-30, 30), c.body.pos[1] + _.random(-30, 30)), c]
                    : [...acc, c]
                : [...acc, c],
        [],
        filtered,
    );
    return {
        cells: R.map(
            (c: Cell.ICell) => Cell.update(c, cells),
            withChildren,
        ),
    };
};

export const store = redux.createStore<IState, any, {}, {}>(
    reducer,
    initialState,
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
);