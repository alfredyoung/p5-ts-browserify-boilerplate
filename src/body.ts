import * as R from 'ramda';

import * as Vector from './vector';
import * as Steer from './steer';
import { HEIGHT, WIDTH, MAX_ACCELERATION, MAX_VELOCITY } from './constants';

export interface IBody {
    pos: Vector.Vector;
    vel: Vector.Vector;
    acc: Vector.Vector;
}

export const of: (pos: Vector.Vector, vel?: Vector.Vector, acc?: Vector.Vector) => IBody =
    (pos, vel = Vector.zero(), acc = Vector.zero()) => ({ acc, vel, pos });

export const random = () => of(Vector.random(WIDTH, HEIGHT));

const clampVelocity = Vector.clamp(-MAX_VELOCITY, MAX_VELOCITY);

const clampAcceleration = Vector.clamp(-MAX_ACCELERATION, MAX_ACCELERATION);

const mapToPositions = R.map(({ pos }: IBody) => pos);

export const updatePosition = R.pipe(
    Vector.sub,
    Vector.roundToPlace(2),
    Vector.round,
    ([x, y]) => Vector.of(
        R.clamp(0, WIDTH, x),
        R.clamp(0, HEIGHT, y),
    ),
);

export const updateVelocity = R.pipe(
    Vector.sub,
    Vector.roundToPlace(2),
    clampVelocity,
);

const roundToTwoPlaces = Vector.roundToPlace(2);

export const updateAcceleration = (pos: Vector.Vector, vel: Vector.Vector, others: IBody[], health: number) => {
    const seperation = Steer.seperate(pos, vel, mapToPositions(others));
    const congregation = Steer.congregate(pos, vel, mapToPositions(others));
    if (others.length > 10) {
        const scaledSeperation = Vector.scale(0.1, seperation);
        const scaledCongregation = Vector.scale(0.9, congregation);
        const sum = Vector.add(scaledCongregation, scaledSeperation);
        const rounded = roundToTwoPlaces(sum);
        const clamped = clampAcceleration(rounded);
        return clamped;
    } else {
        const scaledSeperation = Vector.scale(0.9, seperation);
        const scaledCongregation = Vector.scale(0.1, congregation);
        const sum = Vector.add(scaledCongregation, scaledSeperation);
        const rounded = roundToTwoPlaces(sum);
        const clamped = clampAcceleration(rounded);
        return clamped;
    }
};

