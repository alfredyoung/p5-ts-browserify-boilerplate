import * as _ from 'lodash';
import * as R from 'ramda';

export type Vector = [number, number];

export const of: (x: number, y: number) => Vector =
    (x, y) => [x, y];

export const zero = () => of(0, 0);

export const random = (maxX: number, maxY: number) => of(
    _.random(maxX),
    _.random(maxY),
);

export const map = R.curry(
    (fn: (a: number) => number, [x, y]: Vector) => of(fn(x), fn(y))
);

export const invert = (v: Vector) => map(R.negate, v);

export const round = (v: Vector) => map(_.round, v);

const curryAndFlipPlaces = R.pipe(
    R.flip,
    R.curry,
);

export const roundToPlace = R.curry(
    (places: number, v: Vector) => map(curryAndFlipPlaces(_.round)(places), v)
);

export const scale = R.curry(
    (s: number, v: Vector) => map(R.multiply(s), v)
);

export const clamp = R.curry(
    (min: number, max: number, v: Vector) => map(R.clamp(min, max), v)
);

export const mag = ([x, y]: Vector) => Math.sqrt((x * x) + (y * y));

export const ofMag = R.curry(
    (m: number, v: Vector) => scale(m / mag(v), v)
);

export const add = ([x1, y1]: Vector, [x2, y2]: Vector) => of(
    x1 + x2,
    y1 + y2,
);

export const sub = (v1: Vector, v2: Vector) => add(v1, invert(v2));

const toGrid = map((x: number) => Math.ceil(x / 10) * 10);
const toString = ([x, y]) => `${x}${y}`;

const toGridString = R.pipe(toGrid, toString);

export const dist = R.curry(R.memoizeWith(
    (v1: Vector, v2: Vector) => `${toGridString(v1)}${toGridString(v2)}`,
    R.pipe(
        sub,
        mag,
    ),
));
