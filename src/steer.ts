import * as R from 'ramda';

import * as Vector from './vector';
import { MAX_ACCELERATION } from './constants';

type Position = Vector.Vector;
type Velocity = Vector.Vector;
type Acceleration = Vector.Vector;

const scaleToMax = Vector.ofMag(MAX_ACCELERATION);

const scaleToConverge = (v: Vector.Vector) => Vector.mag(v) < 100
    ? Vector.ofMag((Vector.mag(v) / 100) * MAX_ACCELERATION, v)
    : scaleToMax(v);

const findDesired = R.pipe(
    Vector.sub,
    scaleToConverge,
);

export const seek: (p1: Position, vel: Velocity, p2: Position) => Acceleration =
    (p1, vel, p2) => {
        const desired = findDesired(p1, p2);
        return Vector.sub(desired, vel);
    };

export const flee: (p1: Position, vel: Velocity, p2: Position) => Acceleration =
    (p1, vel, p2) => {
        const desired = Vector.invert(findDesired(p1, p2));
        return Vector.sub(desired, vel);
    };

export const seperate: (self: Position, vel: Velocity, others: Position[]) => Acceleration =
    (self, vel, others) => {
        if (others.length > 0) {
            const sum = R.reduce(
                (s, p) => {
                    const distance = Vector.dist(self, p);
                    const avoidance = Vector.invert(findDesired(self, p));
                    const weighted = Vector.scale(1 / distance, avoidance)
                    return Vector.add(s, weighted);
                },
                Vector.zero(),
                others,
            );
            const avg = Vector.scale(1 / others.length, sum);
            const scaled = scaleToMax(avg);
            return Vector.sub(scaled, vel);
        } else {
            return Vector.zero();
        }
    };

export const congregate: (self: Position, vel: Velocity, others: Position[]) => Acceleration =
    (self, vel, others) => {
        if (others.length > 0) {
            const sum = R.reduce(
                (s, p) => {
                    const distance = Vector.dist(self, p);
                    const avoidance = Vector.invert(findDesired(self, p));
                    const weighted = Vector.scale(1 / distance, avoidance)
                    return Vector.add(s, weighted);
                },
                Vector.zero(),
                others,
            );
            const avg = Vector.scale(1 / others.length, sum);
            const inverted = Vector.invert(avg);
            const scaled = scaleToMax(inverted);
            return Vector.sub(scaled, vel);
        } else {
            return Vector.zero();
        }
    };

// export const seperate: (self: Position, others: Position[]) => Acceleration =
//     (self, others) => {
//         console.log(self, others);

//         const sum = R.reduce(
//             (s, p) => {
//                 const distance = Vector.dist(self, p);
//                 const avoidance = flee(self, p);
//                 const weighted = Vector.scale(1 / distance, avoidance)
//                 return Vector.add(s, weighted);
//             },
//             Vector.zero(),
//             others,
//         );

//         return Vector.scale(1 / others.length, sum);
//     };
