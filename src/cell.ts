import * as _ from 'lodash';
import * as R from 'ramda';

import * as Vector from './vector';
import * as Body from './body';
import { WIDTH, HEIGHT } from './constants';

export interface ICell {
    body: Body.IBody;
    health: number;
}

export const of: (x: number, y: number) => ICell = (x, y) => ({
    body: Body.of(Vector.of(x, y)),
    health: 100,
});

const findNeighbors = (self: ICell, others: ICell[], maxDist: number) =>
    R.filter(
        (c) => {
            const d = Vector.dist(self.body.pos, c.body.pos);
            return d > 1 && d < maxDist;
        },
        others,
    );

export const random = () => of(_.random(WIDTH), _.random(HEIGHT));

const mapToBodies = R.map(({ body }: ICell) => body);

export const update: (cell: ICell, others: ICell[]) => ICell =
    (cell, others) => {
        const neighbors = findNeighbors(cell, others, (150 - cell.health));
        return {
            body: Body.of(
                Body.updatePosition(cell.body.pos, cell.body.vel),
                Body.updateVelocity(cell.body.acc, cell.body.vel),
                Body.updateAcceleration(
                    cell.body.pos,
                    cell.body.vel,
                    mapToBodies(neighbors),
                    cell.health,
                ),
            ),
            health: R.clamp(
                0,
                100,
                neighbors.length < 5
                    ? cell.health - 1
                    : neighbors.length > 40
                        ? cell.health - 1
                        : cell.health + 1,
            ),
        };
    };
