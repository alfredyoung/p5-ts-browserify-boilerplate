import * as p5 from 'p5';
import * as R from 'ramda';

import { HEIGHT, WIDTH } from './constants';
import { store } from './store';
import { ICell } from './cell';

setInterval(() => store.dispatch({
    type: 'STEP',
}), 1000 / 10);

new p5(function (sketch: p5) {

    sketch.drawCell = ({ body, health }: ICell) => {
        sketch.fill(health, (100 - health), health);
        sketch.ellipse(body.pos[0], body.pos[1], 4, 4);
        sketch.fill(health, (100 - health), health);
        sketch.ellipse(body.pos[0], body.pos[1], R.clamp(3, 10, health));
    }

    sketch.setup = function () {
        sketch.createCanvas(WIDTH, HEIGHT);
        sketch.ellipseMode(sketch.CENTER);
        sketch.noStroke();
    }

    sketch.draw = function () {
        const { cells } = store.getState();
        sketch.background(250, 250, 250, 50);
        R.forEach(
            sketch.drawCell,
            cells,
        );
    }

});
