export const WIDTH = 1200;
export const HEIGHT = 800;

export const MAX_VELOCITY = 10;
export const MAX_ACCELERATION = 10;

export const NUM_BODIES = 20;
